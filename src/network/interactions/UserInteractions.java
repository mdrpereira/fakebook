package network.interactions;

import java.util.Iterator;
import java.util.List;

import network.interactions.comments.Comment;
import network.interactions.posts.Post;
import network.user.User;
import utilities.exceptions.DuplicateFriendException;
import utilities.exceptions.NoCommentsException;
import utilities.exceptions.NoFriendsException;
import utilities.exceptions.NoPostsException;

/**
 * Responsible for the storage of all user interactions with fakebook, instantiating the <code>Friend</code>, <code>Post</code> and <code>Comment</code> hierarchy. <br>
 * Practically serves as a main repository for all information, with minimal runtime, and maximum extensibility. <br>
 * All information stored is user-based/dependent. <br>
 * 
 * @author Manuel Pereira 
 * @author Mauricio Polvora
 */
public interface UserInteractions {
	
	/**
	 * Adds a <code>User</code> as the users friend. <br>
	 * If the friend already exists, an exception is thrown.
	 * 
	 * @param friend - user to be added as a friend. 
	 * @throws DuplicateFriendException thrown if <code>User</code> to be added is repeated.
	 */
	public void addFriend(User friend) throws DuplicateFriendException;
	
	/**
	 * Adds a post to the user posts..
	 * 
	 * @param userId - The user Id that posted 
	 * @param hashtags - The hashtags added.
	 * @param truthfulness - Truthfulness of the post. 
	 * @param message - The message of the post itself.
	 */
	public void post (String userId, List<String> hashtags, String truthfulness, String message);
	
	/**
	 * Receives a post from their friends
	 * 
	 * @param shared - The post posted by one of their friends.
	 */
	public void receive (Post shared);
	
	/**
	 * User comments on a post in the network. The comment is then saved by subject in a database composed of all <code>User</code> comments.
	 * 
	 * @param subjects - The subject of the comment.
	 * @param comment - The comment itself
	 */
	public void comment(List<String> subjects, Comment comment);
	
	//================================================================================
	
	/**
	 * Returns the last <code>Post</code> posted by the user.
	 * 
	 * @return - postId (int)
	 */
	
	public int getLastId();
	
	//================================================================================
	
	/**
	 * Checks if the user has a specific post in the wall
	 * 
	 * @param shared - <code>Post</code> to be checked
	 * @return - True if it has.
	 */
	public boolean hasSharedPost(Post shared);
	
	//================================================================================
	
	/**
	 * Returns a specific <code>Post</code> by its post id
	 * 
	 * @param postId - The post id of the <code>Post</code> we want to get
	 * @return - The <code>Post</code> itself
	 */
	public Post getPost (int postId);
	
	//================================================================================
	
	/**
	 * Returns an iterator, conditioned by the users currently declared as this user friends. <br>
	 * If there are no friends, an exception is thrown.
	 * 
	 * @return the iterator of the current friend collection.
	 * @throws NoFriendsException thrown if there are no friends.
	 */
	public Iterator<User> friendIterator() throws NoFriendsException;
	
	/**
	 * Returns an iterator of all the posts of the <code>User</code>
	 * 
	 * @return - The iterator of all the posts
	 * @throws NoPostsException - thrown if there are no <code>Posts</code> in the user wall
	 */
	public Iterator<Post> postIterator() throws NoPostsException;
	
	/**
	 * Returns an iterator of all accessible posts by the user, this contains the posted and shared.
	 * 
	 * @return - The iterator of all the available posts including the shared
	 */
	public Iterator<Post> availablePosts();
	
	public Iterator<Post> allPosts();
	
	/**
	 * Returns an iterator of all the user comments in <code>Posts</code> with given subject
	 * 
	 * @param subject
	 * @return - The iterator of all the comments.
	 * @throws NoCommentsException - thrown if the <code>User</code> didnt comment
	 */
	public Iterator<Comment> userComments(String subject) throws NoCommentsException;
	
	/**
	 * Returns an iterator of all the user comments in all the <code>User</code> <code>Posts</code>
	 * 
	 * @return - The iterator of all the comments.
	 */
	public Iterator<Comment> allComments();
	
	
}

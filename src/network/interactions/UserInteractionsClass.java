package network.interactions;

import java.util.Iterator;
import java.util.List;

import network.interactions.comments.Comment;
import network.interactions.comments.UserComments;
import network.interactions.comments.UserCommentsClass;
import network.interactions.friends.Friend;
import network.interactions.friends.FriendsClass;
import network.interactions.posts.Post;
import network.interactions.posts.Wall;
import network.interactions.posts.WallClass;
import network.user.User;
import utilities.exceptions.DuplicateFriendException;
import utilities.exceptions.NoCommentsException;
import utilities.exceptions.NoFriendsException;
import utilities.exceptions.NoPostsException;

/**
 * Responsible for the storage of all user interactions with fakebook, instantiating the <code>Friend</code>, <code>Post</code> and <code>Comment</code> hierarchy. <br>
 * Pratctically serves as a main repository for all information, with minimal runtime, and maximum extensibility. <br>
 * All information stored is user-based/dependent. <br>
 * 
 * @author Manuel Pereira 
 * @author Mauricio Polvora
 */
public class UserInteractionsClass implements UserInteractions{
	private Friend friends;
	private Wall wall;
	private UserComments comments;

	//================================================================================
	
	public UserInteractionsClass() {
		friends = new FriendsClass();
		wall = new WallClass();
		comments = new UserCommentsClass();
	}
	
	//================================================================================

	public void addFriend(User friend) throws DuplicateFriendException{
		friends.addFriend(friend);
	}

	
	public void post (String userId, List<String> hashtags, String truthfulness, String message) {
		wall.post(userId, hashtags, truthfulness, message);
	}
	
	public void receive (Post shared) {
		wall.receive(shared);
	}
	
	public void comment(List<String> subjects, Comment comment) {
		comments.comment(subjects, comment);
	}
	
	//================================================================================
	
	public int getLastId() {
		return (wall.getLastId());
	}
	
	//================================================================================
	
	public boolean hasSharedPost(Post post) {
		return wall.hasSharedPost(post);
	}
	
	//================================================================================
	
	public Post getPost (int postId) {
		return (wall.getPost(postId));
	}
	
	//================================================================================

	/**
	 * {@inheritDoc}
	 */
	public Iterator<User> friendIterator() throws NoFriendsException {
		return friends.friendIterator();
	}
	
	public Iterator<Post> postIterator() throws NoPostsException {
		return wall.postIterator();
	}
	
	public Iterator<Post> availablePosts() {
		return wall.availablePosts();
	}
	
	public Iterator<Post> allPosts() {
		return wall.allPosts();
	}
	
	public Iterator<Comment> userComments(String subject) throws NoCommentsException {
		return comments.userComments(subject);
	}
	
	public Iterator<Comment> allComments() {
		return comments.allComments();
	}
	
	//================================================================================
}

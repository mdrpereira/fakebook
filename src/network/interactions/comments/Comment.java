package network.interactions.comments;

/**
 * Comment class initiated when a <code>User</code> posts a comment
 * 
 * @author Manuel Pereira
 * @author Mauricio Polvora
 */
public interface Comment {
	

	/**
	 * Returns the <code>post</code> id that has the <code>comment</code>
	 * 
	 * @return <code>post</code> id
	 */
	public int getPostId();

	//================================================================================
	
	/**
	 * Returns the <code>User</code> id that posted the <code>Post</code> in which the comment was made
	 * 
	 * @return <code>User</code> id
	 */
	public String getPosterId();

	/**
	 * Returns the <code>User</code> id that posted the <code>comment</code>
	 * 
	 * @return <code>User</code> id
	 */
	public String getCommenterId();
	
	/**
	 * Returns the <code>post</code> truthfulness that has the <code>comment</code>
	 * 
	 * @return <code>post</code> truthfulness
	 */
	public String getPostTruthfulness();
	
	/**
	 * Returns the <code>post</code> stance that has the <code>User</code> of the <code>post</code> has 
	 * 
	 * @return Poster stance
	 */
	public String getStance();
	
	/**
	 * Returns the comment itself
	 * 
	 * @return the <code>comment</code>
	 */
	public String getComment();
	
	//================================================================================
	
	/**
	 * Returns if the <code>comment</code> is positive
	 * 
	 * @return true if it is
	 */
	public boolean isPositive();
	
	/**
	 * Returns if the <code>post</code> in which the <code>comment</code> was made is truthful
	 * 
	 * @return true if it is
	 */
	public boolean postIsTruthful();
}

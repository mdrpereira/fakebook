package network.interactions.comments;

import utilities.enumerates.stances.CommentEnum;
import utilities.enumerates.stances.PostEnum;

public class CommentClass implements Comment{
	private String posterId, commenterId, postTruthfulness, stance, comment;
	private int postId;
	
	//================================================================================
	
	public CommentClass(String posterId, String commenterId, int postId, String postTruthfulness, String stance, String comment) {
		this.posterId = posterId;
		this.commenterId = commenterId;
		this.postId = postId;
		this.postTruthfulness = postTruthfulness;
		this.stance = stance;
		this.comment = comment;
	}
	
	//================================================================================
	
	public int getPostId() {
		return postId;
	}
	
	//================================================================================
	
	public String getPosterId() {
		return posterId;
	}
	
	public String getCommenterId() {
		return commenterId;
	}
	
	public String getPostTruthfulness() {
		return postTruthfulness;
	}
	
	public String getStance() {
		return stance;
	}
	
	public String getComment() {
		return comment;
	}
	
	//================================================================================
	
	public boolean isPositive() {
		return CommentEnum.isPositive(stance);
	}
	
	public boolean postIsTruthful() {
		return PostEnum.isHonest(postTruthfulness);
	}
}
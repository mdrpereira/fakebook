package network.interactions.comments;

import java.util.Iterator;
import java.util.List;

import utilities.exceptions.NoCommentsException;

/**
 * Class that registers and manages all comments in the system
 * 
 * @author Manuel Pereira
 * @author Mauricio Polvora
 */
public interface UserComments {

	/**
	 * Registers a new comment in the system.
	 * 
	 * @param subjects - The subjects of the comment
	 * @param comment - The comment itself
	 */
	public void comment (List<String> subjects, Comment comment);
	
	//================================================================================
	
	/**
	 * Returns a <code>Iterator</code> with comments from a specific subject
	 * 
	 * @param subject - The subject we want to take the comments from
	 * @return - <code>Iterator</code>
	 * @throws NoCommentsException - thrown if there are no comments with that subject
	 */
	public Iterator<Comment> userComments(String subject) throws NoCommentsException;
	
	/*
	 * Returns a <code>Iterator</code> with all comments
	 * 
	 * @return - All comments <code>Iterator</code>
	 */
	public Iterator<Comment> allComments();
}

package network.interactions.comments;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import utilities.exceptions.NoCommentsException;

public class UserCommentsClass implements UserComments {
	private Map<String, Set<Comment>> comments;

	//================================================================================
	
	public UserCommentsClass() {
		comments = new HashMap<String, Set<Comment>>();
	}

	//================================================================================
	
	public void comment (List<String> subjects, Comment comment) {
		for (String subject: subjects) {
			if (!comments.containsKey(subject)) {
				comments.put(subject, new LinkedHashSet<Comment>());
			}
			 comments.get(subject).add(comment);
		}
	}
	
	//================================================================================
	
	public Iterator<Comment> userComments(String subject) throws NoCommentsException {
		if (comments.isEmpty() || !comments.containsKey(subject) || comments.get(subject).isEmpty()) {
			throw new NoCommentsException();
		}
		return (comments.get(subject).iterator());
	}
	
	public Iterator<Comment> allComments() {
		Set<Comment> temp = new HashSet<Comment>();
		
		for (String subject: comments.keySet()) {
			temp.addAll(comments.get(subject));
		}
		
		return temp.iterator();
	}

}

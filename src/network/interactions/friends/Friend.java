package network.interactions.friends;

import java.util.Iterator;

import network.user.User;
import utilities.exceptions.DuplicateFriendException;
import utilities.exceptions.NoFriendsException;
/**
 * 
 * 
 * @author Mauricio Polvora
 * @author Manuel Pereira
 */
public interface Friend {

	public void addFriend(User friend) throws DuplicateFriendException;
	
	//================================================================================
	
	public Iterator<User> friendIterator() throws NoFriendsException;

}

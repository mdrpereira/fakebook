package network.interactions.friends;

import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

import network.user.User;
import utilities.comparators.UserIdComparator;
import utilities.exceptions.DuplicateFriendException;
import utilities.exceptions.NoFriendsException;

public class FriendsClass implements Friend {
	private Set<User> friends;
	
	//================================================================================
	
	public FriendsClass() {
		friends = new TreeSet<User>(new UserIdComparator());
	}
	
	//================================================================================
	
	public void addFriend(User friend) throws DuplicateFriendException {
		if (friends.contains(friend)) {
			throw new DuplicateFriendException();
		}
		
		friends.add(friend);
	}
	
	//================================================================================
	
	public Iterator<User> friendIterator() throws NoFriendsException {
		if (friends.isEmpty()) {
			throw new NoFriendsException();
		}
		return friends.iterator();
	}
}

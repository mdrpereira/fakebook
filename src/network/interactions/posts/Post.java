package network.interactions.posts;
import java.util.List;

import network.interactions.comments.Comment;
public interface Post {

	public void comment (String commenterId, String stance, String comment);

	//================================================================================

	public String getPosterId();

	public String getTruthfulness();

	public String getMessage();

	//================================================================================

	public int getCommentsAmount();

	public int getPostId();

	//================================================================================

	public boolean isTruthful();

	public boolean userCommented (String userId);

	//================================================================================

	public Comment getLastComment();

	//================================================================================

	public List<String> getHashtags();

	public List<Comment> getComments();

}

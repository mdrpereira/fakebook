package network.interactions.posts;
import java.util.ArrayList;
import java.util.List;

import network.interactions.comments.Comment;
import network.interactions.comments.CommentClass;
import utilities.enumerates.stances.PostEnum;

public class PostClass implements Post{
	String userId, truthfulness, message;
	int postId, commentsAmt;
	
	private List<String> hashtags;
	private List<Comment> comments;
	
	//================================================================================
	
	public PostClass (int postId, String userId, List<String> hashtags, String truthfulness, String message) {
		this.postId = postId;
		this.userId = userId;
		this.truthfulness = truthfulness;
		this.message = message;
		this.hashtags = new ArrayList<String>(hashtags);
		this.comments = new ArrayList<Comment>();
	}
	
	//================================================================================
	
	public void comment (String commenterId, String stance, String comment) {
		comments.add(new CommentClass(userId, commenterId, postId, truthfulness, stance, comment));
		commentsAmt++;
	}
	
	//================================================================================
	
	public String getPosterId() {
		return userId;
	}
	
	public String getTruthfulness() {
		return truthfulness;
	}
	
	public String getMessage() {
		return message;
	}
	
	//================================================================================
	
	public List<String> getHashtags() {
		return hashtags;
	}
	
	public List<Comment> getComments() {
		return comments;
	}
	
	//================================================================================
	
	public int getCommentsAmount() {
		return commentsAmt;
	}
	
	public int getPostId() {
		return postId;
	}
	
	//================================================================================
	
	public boolean isTruthful() {
		return PostEnum.isHonest(truthfulness);
	}
	
	public boolean userCommented (String userId) {
		boolean res = false;
		
		for (Comment comment: comments) {
			if (comment.getCommenterId().equals(userId)) {
				res = true;
			}
		}
		return res;
	}
	
	//================================================================================
	
	public Comment getLastComment() {
		return (comments.get(commentsAmt - 1));
	}
}

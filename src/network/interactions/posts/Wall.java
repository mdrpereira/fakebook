package network.interactions.posts;
import java.util.Iterator;
import java.util.List;

import utilities.exceptions.NoPostsException;

/**
 * Class to store all posts from a <code>User</code>
 * 
 * @author Mauricio Polvora
 * @author Manuel Pereira
 *
 */
public interface Wall {

	/**
	 * Posts a new post in the system.
	 * 
	 * @param userId - <code>User</code> that posted the post
	 * @param hashtags - <code>hashtags</code> of the post.
	 * @param truthfulness - <code>truthfulness</code> of the post
	 * @param message - The message content itself
	 */
	public void post (String userId, List<String> hashtags, String truthfulness, String message);
	
	/**
	 * Receives a post from one of their friends and stores it.
	 * 
	 * @param sharedPost - The received <code>Post</code>
	 */
	public void receive (Post sharedPost);

	//================================================================================
	
	/**
	 * Returns the last <code>Post</code> id that was registered in the system,
	 * 
	 * @return - Last registered <code>Post</code> id (int)
	 */
	public int getLastId();
	
	//================================================================================
	
	/**
	 * Checks if the user has a specific post in the wall
	 * 
	 * @param shared - <code>Post</code> to be checked
	 * @return - True if it has.
	 */
	public boolean hasSharedPost(Post post);
	
	//================================================================================
	
	/**
	 * Returns a specific <code>Post</code> by its post id
	 * 
	 * @param postId - The post id of the <code>Post</code> we want to get
	 * @return - The <code>Post</code> itself
	 */
	public Post getPost(int postId);
	
	//================================================================================
	/**
	 * Returns an iterator of all the posts of the <code>User</code>
	 * 
	 * @return - The iterator of all the posts
	 * @throws NoPostsException - thrown if there are no <code>Posts</code> in the user wall
	 */
	public Iterator<Post> postIterator() throws NoPostsException;
	
	/**
	 * Returns an iterator of all accessible posts by the user, this contains the posted and shared.
	 * 
	 * @return - The iterator of all the available posts including the shared
	 */
	public Iterator<Post> availablePosts();
	
	public Iterator<Post> allPosts();
}

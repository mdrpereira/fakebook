package network.interactions.posts;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import utilities.exceptions.NoPostsException;

public class WallClass implements Wall {
	private Map<Integer,Post> posted;
	private List<Post> shared;

	private int currentId;

	//================================================================================
	
	public WallClass() {
		posted = new TreeMap<Integer,Post>();
		shared = new ArrayList<Post>();
		currentId = 1;
	}

	//================================================================================
	
	public void post (String userId, List<String> hashtags, String truthfulness, String message) {
		posted.put(currentId, new PostClass(currentId++, userId, hashtags, truthfulness, message));
	}

	public void receive (Post sharedPost) {
		shared.add(sharedPost);
	}
	
	//================================================================================
	
	public int getLastId() {
		return (((TreeMap<Integer,Post>) posted).lastKey());
	}

	//================================================================================
	
	public boolean hasSharedPost(Post post) {
		return (shared.contains(post));
	}
	
	//================================================================================
	
	public Post getPost (int postId) {
		return (posted.get(postId));
	}
	
	//================================================================================
	
	public Iterator<Post> postIterator() throws NoPostsException {
		if (posted.isEmpty()) {
			throw new NoPostsException();
		}
		Collection<Post> posts = posted.values();
		
		return posts.iterator();
	}
	
	public Iterator<Post> availablePosts() {
		Set<Post> posts = new HashSet<Post>(shared);
		
		for (Integer i: posted.keySet()) {
			posts.add(posted.get(i));
		}
		
		return posts.iterator();
	}
	
	public Iterator<Post> allPosts() {
		return posted.values().iterator();
	}
}

package network.topLevel;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import network.interactions.comments.Comment;
import network.interactions.posts.Post;
import network.user.User;
import network.user.userKinds.fanatic.Fanaticism;
import network.user.userKinds.fanatic.FanaticismClass;
import utilities.enumerates.CommandEnum;
import utilities.enumerates.OutputEnum;
import utilities.enumerates.UserKindEnum;
import utilities.exceptions.DuplicateFriendException;
import utilities.exceptions.DuplicateIdException;
import utilities.exceptions.DuplicateUserException;
import utilities.exceptions.InadequateCommentStanceException;
import utilities.exceptions.InadequatePostStanceException;
import utilities.exceptions.InexistentPostException;
import utilities.exceptions.InvalidElementNumberException;
import utilities.exceptions.InvalidFanaticismException;
import utilities.exceptions.InvalidHashtagsException;
import utilities.exceptions.InvalidSubjectException;
import utilities.exceptions.InvalidUserException;
import utilities.exceptions.InvalidUserKindException;
import utilities.exceptions.NoCommentsException;
import utilities.exceptions.NoElementsException;
import utilities.exceptions.NoFriendsException;
import utilities.exceptions.NoPostsException;
import utilities.exceptions.NoUsersException;
import utilities.exceptions.PostNotReceivedException;
import utilities.exceptions.RepeatedFanaticismsException;
import utilities.exceptions.SelfcenteredException;

/**
 * Henlo | send toe 
 * @author Manuel Pereira
 * @author Mauricio Polvora
 */
public class Main {

	public static void main(String[] args) {
		Manager man = new ManagerClass();
		Scanner in = new Scanner(System.in);

		String select = readCommand(in);

		while (!select.equals(CommandEnum.EXIT.name())) {
			
			processCommand(in, man, select);
			select = readCommand(in);
		}

		System.out.println(OutputEnum.EXIT.getMessage());
		
		in.close(); 
	}

	/**
	 * Returns a value read from the console, transformed to upper-case to eliminate case-based reading errors. <br>
	 * Serves as main program input, reading all system-level method calls.
	 * 
	 * @param input - <code>java.util</code> standard scanner, takes in values from console (<code>stdio</code>).
	 * @return next line of input to upper-case. 
	 */
	private static String readCommand (Scanner input) {
		return (input.next().toUpperCase());
	}

	/**
	 * Calls system-level methods according to user input. <br>
	 * Decision is made according to the input value, in comparison with <code>Command</code> enumerate.
	 * 
	 * @param in - <code>java.util</code> standard scanner. Solely for furthering input capabilities into sub-methods.
	 * @param man - <code>Manager</code> interface, implemented in the top-level class <code>ManagerClass</code>. Establishes connection to lower classes. 
	 * @param cmdIN - input <code>String</code>, used for method-calling.
	 */
	@SuppressWarnings("incomplete-switch")
	private static void processCommand (Scanner in, Manager man, String cmdIN) {
		try {
			CommandEnum cmd = CommandEnum.valueOf(cmdIN);

			switch (cmd) {

			case REGISTER:
				register(in, man);
				break;

			case USERS:
				users(man);
				break;

			case ADDFRIEND:
				addFriend(in, man);
				break;

			case FRIENDS:
				friends(in, man);
				break;

			case POST:
				post(in, man);
				break;

			case USERPOSTS:
				userposts(in, man);
				break;

			case COMMENT:
				comment(in, man);
				break;

			case READPOST:
				readpost(in, man);
				break;

			case COMMENTSBYUSER:
				commentsByUser(in, man);
				break;

			case TOPICFANATICS:
				topicFanatics(in, man);
				break;

			case TOPICPOSTS:
				topicPosts(in, man);
				break;

			case POPULARPOST:
				popularPost(man);
				break;

			case TOPPOSTER:
				topPoster(man);
				break;

			case RESPONSIVE:
				responsive(man);
				break;

			case SHAMELESS:
				shameless(man);
				break;

			case HELP:
				help();
				break;
			}
		}
		catch (IllegalArgumentException e) {
			in.nextLine();
			System.out.println(OutputEnum.INVALID_COMMAND.getMessage());
		}
	}

	/**
	 * Method that registers a new <code>User</code> in the system. It takes the information about said user in <code>in</code> and uploads it to 
	 * <code>man</code>
	 * 
	 * @param in - Takes the details of the <code>User</code> to be registered. <code>userKind</code>, <code>userId</code> , <code>stance</code>
	 * and <code>subject</code>
	 * @param man - Uploads this details to the system manager.
	 */
	private static void register (Scanner in, Manager man) {
		String userKind, userId, stance, subject;;
		int numFan; 

		try {
			userKind = in.next();
			userId = in.nextLine().trim();

			if (userKind.equalsIgnoreCase(UserKindEnum.fanatic.name())) {
				List<Fanaticism> fanaticisms = new ArrayList<Fanaticism>();
				numFan = in.nextInt();

				for (int i = 0; i < numFan; i++) {
					stance = in.next();
					subject = in.next();

					fanaticisms.add(new FanaticismClass(stance, subject));
				}
				man.register(userKind, userId, fanaticisms);
			}
			else {
				man.register(userKind, userId);
			}

			System.out.println(userId + OutputEnum.REGISTERED.getMessage());
		}
		catch (InvalidUserKindException invEx) {
			System.out.println(invEx.getMessage());
		}
		catch (DuplicateUserException dupEx) {
			System.out.println(dupEx.getMessage());
		}
		catch (RepeatedFanaticismsException repEx) {
			System.out.println(repEx.getMessage());
		}
	}

	/**
	 * Method that lists all users in the system.
	 * 
	 * @param man - Takes the information from the manager and prints it.
	 */
	private static void users (Manager man) {
		try {
			Iterator<User> it = man.userIterator();

			while (it.hasNext()) {
				User u = it.next();

				System.out.println(u.getUserId() + " [" + u.getUserKind() + "] " + u.getFriendsAmount() + " " + u.getPostsAmount() + " " + u.getCommentsAmount());
			}
		}
		catch (NoUsersException noUsersEx) {
			System.out.println(noUsersEx.getMessage());
		}
	}

	/**
	 * Method that adds a new friend to a given <code>User</code>.
	 * 
	 * @param in - Takes the details of the two users that will become friends
	 * @param man -  Sends the information to the system manager and creates the friendship.
	 */
	private static void addFriend (Scanner in, Manager man) {
		String user1Id, user2Id;

		user1Id = in.nextLine().trim();
		user2Id = in.nextLine().trim();

		try {
			man.addFriend(user1Id, user2Id);
			System.out.println(user1Id + OutputEnum.FRIEND_ADDED.getMessage() + user2Id + ".");
		}
		catch (InvalidUserException invUserEx) {
			System.out.println(invUserEx.getMessage());
		}
		catch (DuplicateIdException dupIdEx) {
			System.out.println(dupIdEx.getMessage());
		}
		catch (DuplicateFriendException dupFriendEx) {
			System.out.println(dupFriendEx.getMessage());
		}
	}

	/**
	 * Method that lists a <code>User</code> friends.
	 * 
	 * @param in - Takes the information about the <code>User</code> we want to access.
	 * @param man - Sends the information to the system manager and prints the friends.
	 */
	private static void friends (Scanner in, Manager man) {
		String userId;

		userId = in.nextLine().trim();

		try {
			Iterator<User> it = man.friendIterator(userId);
			String friends = "";

			while (it.hasNext()) {
				User u = it.next();

				if (it.hasNext()) {
					friends += (u.getUserId() + ", ");
				}
				else { 
					friends += (u.getUserId() + ".");
				}
			}
			System.out.println(friends);
		}
		catch (InvalidUserException invUserEx) {
			System.out.println(invUserEx.getMessage());
		}
		catch (NoFriendsException noFriendsEx) {
			System.out.println(noFriendsEx.getMessage());
		}
	}

	/**
	 * Method that posts a new <code>User</code> message.
	 * 
	 * @param in - Takes information about the post, <code>userId</code>, <code>truthfulness</code> and <code>message</code>
	 * @param man - sends the information to the system manager and registers the post.
	 */
	private static void post (Scanner in, Manager man) {
		String userId, truthfulness, message;
		int numHash;
		List<String> hashtags = new ArrayList<String>();

		userId 			= in.nextLine().trim();
		numHash			= in.nextInt();

		for (int i = 0; i < numHash; i++) {
			String hashtag = in.next();

			hashtags.add(hashtag);
		}

		truthfulness 	= in.next();	
		message 		= in.nextLine().trim(); 

		try {
			man.post(userId, numHash, hashtags, truthfulness, message);

			int friends = man.getFriendsAmount(userId);
			int postId = man.getLatestPostId(userId);

			System.out.println(userId + OutputEnum.POSTED_1.getMessage() + truthfulness + OutputEnum.POSTED_2.getMessage() + friends + OutputEnum.POSTED_3.getMessage() + postId + ".");
		}
		catch (InvalidUserException invUserEx) {
			System.out.println(invUserEx.getMessage());
		}
		catch (InvalidHashtagsException invHashEx) {
			System.out.println(invHashEx.getMessage());
		}
		catch(InadequatePostStanceException inadStanceEx) {
			System.out.println(inadStanceEx.getMessage());
		}	
	}

	/**
	 * Method that lists all post by an <code>User</code>.
	 * 
	 * @param in - Takes the <code>userId</code> of the <code>User</code> we want to access the posts from.
	 * @param man - Sends that information the manager.
	 */
	private static void userposts (Scanner in, Manager man) {
		String userId;

		userId = in.nextLine().trim();

		try {
			Iterator<Post> it = man.postIterator(userId);

			System.out.println(userId + " posts:");
			
			while (it.hasNext()) {
				Post post = it.next();

				System.out.println(post.getPostId() + ". [" + post.getTruthfulness() + "] " + post.getMessage() + " [" + post.getCommentsAmount() + " comments]");
			}

		}
		catch (InvalidUserException invUserEx) {
			System.out.println(invUserEx.getMessage());
		}
		catch (NoPostsException noPostsEx) {
			System.out.println(noPostsEx.getMessage());
		}
	}
	
	
	/**
	 * Method that comments a <code>User</code> post.
	 * 
	 * @param in - Takes the information of the commenter and comment details. <code>commenterId</code>, <code>userId</code>, 
	 * <code>stance</code>, <code>comment</code>, <code>postId</code> 
	 * @param man - Sends the information to the system manager and registers the comment in the post.
	 */
	public static void comment (Scanner in, Manager man) {
		String commenterId, userId, stance, comment;
		int postId;
		
		commenterId = in.nextLine().trim();
		userId 		= in.nextLine().trim();
		postId		= in.nextInt();
		stance 		= in.next();
		comment		= in.nextLine().trim();
		
		try {
			man.comment(commenterId, userId, postId, stance, comment);
			System.out.println(OutputEnum.COMMENTED.getMessage());
		}
		catch (InvalidUserException invUserEx) {
			System.out.println(invUserEx.getMessage());
		}
		catch (InexistentPostException inexPostEx) {
			System.out.println(inexPostEx.getMessage());
		}
		catch (PostNotReceivedException noRecEx) {
			System.out.println(noRecEx.getMessage());
		}
		catch (SelfcenteredException selfEx) {
			System.out.println(selfEx.getMessage());
		}
		catch (InadequateCommentStanceException inadStanceEx) {
			System.out.println(inadStanceEx.getMessage());
		}
	}
	
	
	/**
	 * Method that prints detailed information about a post
	 * 
	 * @param in - Takes the information of post we want to read.
	 * @param man - Sends the information to the system manager and prints the information of the post.
	 */
	private static void readpost (Scanner in, Manager man) {
		String userId;
		int postId;
		
		userId = in.nextLine().trim();
		postId = in.nextInt();
		
		try {
			Post post = man.getPost(userId, postId);
			
			System.out.println("[" + post.getPosterId() + " " + post.getTruthfulness() + "] " + post.getMessage());
			
			Iterator<Comment> it = man.readpost(userId, postId);
			
			while (it.hasNext()) {
				Comment com = it.next();
				System.out.println("[" + com.getCommenterId() + " " + com.getStance() + "] " + com.getComment());
			}	
		}
		catch (InvalidUserException invUserEx) {
			System.out.println(invUserEx.getMessage());
		}
		catch (InexistentPostException inexPostEx) {
			System.out.println(inexPostEx.getMessage());
		}
		catch (NoCommentsException noComEx) {
			System.out.println(noComEx.getMessage());
		}
	}
	
	/**
	 * Method that shows all the comments by an user on a particular topic.
	 * 
	 * @param in - Takes the <code>userId</code> of the <code>User</code> we want to access the comments
	 * @param man - Sends the information to the system manager and prints the comments.
	 */
	private static void commentsByUser (Scanner in, Manager man) {
		String userId, subject;
		
		userId = in.nextLine().trim();
		subject = in.next();
		
		try {
			Iterator<Comment> it = man.userComments(userId, subject);
			
			while (it.hasNext()) {
				Comment comment = it.next();
				
				System.out.println("[" + comment.getPosterId() + " " + comment.getPostTruthfulness() + " " + comment.getPostId() + " " + comment.getStance() + "] " + comment.getComment());
			}
		}
		catch (InvalidUserException invUserEx) {
			System.out.println(invUserEx.getMessage());
		}
		catch (NoCommentsException noComEx) {
			System.out.println(noComEx.getMessage());
		}
	}
	
	/**
	 * Method that shows a list of <code>fanatics</code> users on a given topic.
	 * 
	 * @param in - Takes in the <code>subject</code> that we want to get the fanatics from.
	 * @param man - Sends the information to the system manager and prints the fanatics.
	 */
	private static void topicFanatics (Scanner in, Manager man) {
		String subject;
		
		subject = in.next();
		
		try {
			Iterator<User> it = man.topicFanatics(subject);
			String fanatics = "";
			
			while (it.hasNext()) {
				User fanatic = it.next();
				
				if (it.hasNext()) {
					fanatics += (fanatic.getUserId() + ", ");
				}
				else { 
					fanatics += (fanatic.getUserId() + ".");
				}
			}
			System.out.println(fanatics);
		}
		catch (InvalidFanaticismException invFanEx) {
			System.out.println(invFanEx.getMessage());
		}
	}
	
	/**
	 * Method that shows a list of <code>Posts</code> on a given topic.
	 * 
	 * @param in - takes in the <code>subject</code> and the maximum number of posts to list , <code>elems</code>
	 * @param man - Sends that information to the system manager and prints the list of <code>Posts</code>
	 */
	private static void topicPosts (Scanner in, Manager man) {
		String subject;
		int elems;
		
		subject = in.next();
		elems = in.nextInt();
		
		try {
			Iterator<Post> it = man.topicPosts(subject, elems);
			
			while (it.hasNext()) {
				Post post = it.next();
				
				System.out.println(post.getPosterId() + " " + post.getPostId() + " " + post.getCommentsAmount() + ": " + post.getMessage());
			}
		}
		catch (InvalidElementNumberException invElemNumEx ) {
			System.out.println(invElemNumEx.getMessage());
		}
		catch (InvalidSubjectException invSubjEx) {
			System.out.println(invSubjEx.getMessage());
		}
	}
	
	/**
	 * Shows the most commented <code>Post</code>
	 * 
	 * @param man Prints the information of the most commented <code>Post</code>
	 */
	private static void popularPost (Manager man) {
		try {
		Post popular = man.popularPost();
		
		System.out.println(popular.getPosterId() + " " +  popular.getPostId() + " " + popular.getCommentsAmount() + ": " + popular.getMessage());
		}
		catch (NoElementsException noElemEx) {
			System.out.println(noElemEx.getMessage());
		}
	}
	
	/**
	 * Method that prints the <code>User</code> with the highest number of <code>posts</code>
	 * 
	 * @param man - Prints detailed information about the <code>User</code>
	 */
	private static void topPoster (Manager man) {
		try {
			User top = man.topPoster();
			
			System.out.println(top.getUserId() + " " + top.getPostsAmount() + " " + top.getCommentsAmount() + ".");
		}
		catch (NoElementsException noElemEx) {
			System.out.println(noElemEx.getMessage());
			
		}
	}
	
	/**
	 * Method that prints the <code>User</code> with the highest percentage of commented posts.
	 * 
	 * @param man - Prints detailed information about the <code>User</code>
	 */
	private static void responsive (Manager man) {
		try {
		User responsive = man.responsive();
		
		System.out.println(responsive.getUserId() + " " + responsive.getCommentedPosts() + " " + responsive.getAvailablePosts() + ".");
		}
		catch (NoElementsException noElemEx) {
			System.out.println(noElemEx.getMessage());
		}
	}
	
	/**
	 * Method that prints the top liar.
	 * 
	 * @param man -  Prints detailed information about said liar.
	 */
	private static void shameless (Manager man) {
		try {
			User shameless = man.shameless();
			
			System.out.println(shameless.getUserId() + " " + shameless.getTotalLies() + ".");
		}
		catch (NoElementsException noElemEx) {
			System.out.println(noElemEx.getMessage());
		}
	}



	/**
	 * Cycles through all values stored in the <code>Command</code> enumerate, printing out the name and description stored for each element.<br>
	 * Further implementations of system-level methods must first be introduced in the aforementioned enumerate.
	 */
	private static void help() {
		for (CommandEnum help : CommandEnum.values()) {

			System.out.println( help.getName() + " - " + help.getDescription());
		}
	}

}

package network.topLevel;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import network.interactions.comments.Comment;
import network.interactions.posts.Post;
import network.topLevel.bySubject.BySubject;
import network.topLevel.bySubject.BySubjectClass;
import network.user.User;
import network.user.UserCollection;
import network.user.UserCollectionClass;
import network.user.userKinds.fanatic.Fanaticism;
import utilities.enumerates.UserKindEnum;
import utilities.exceptions.DuplicateFriendException;
import utilities.exceptions.DuplicateIdException;
import utilities.exceptions.DuplicateUserException;
import utilities.exceptions.InadequateCommentStanceException;
import utilities.exceptions.InadequatePostStanceException;
import utilities.exceptions.InexistentPostException;
import utilities.exceptions.InvalidElementNumberException;
import utilities.exceptions.InvalidFanaticismException;
import utilities.exceptions.InvalidHashtagsException;
import utilities.exceptions.InvalidSubjectException;
import utilities.exceptions.InvalidUserException;
import utilities.exceptions.InvalidUserKindException;
import utilities.exceptions.NoCommentsException;
import utilities.exceptions.NoElementsException;
import utilities.exceptions.NoFriendsException;
import utilities.exceptions.NoPostsException;
import utilities.exceptions.NoUsersException;
import utilities.exceptions.PostNotReceivedException;
import utilities.exceptions.RepeatedFanaticismsException;
import utilities.exceptions.SelfcenteredException;

public class ManagerClass implements Manager {

	//================================================================================

	private UserCollection userCol;
	private BySubject bySub;

	//================================================================================

	public ManagerClass() {
		userCol = new UserCollectionClass();
		bySub = new BySubjectClass();
	}

	//================================================================================


	public void register (String userKind, String userId) throws InvalidUserKindException, DuplicateUserException {
		if (!UserKindEnum.isValid(userKind)) {
			throw new InvalidUserKindException(userKind);
		}
		userCol.register(userKind, userId);
	}

	public void register (String userKind, String userId, List<Fanaticism> fanaticisms) throws RepeatedFanaticismsException, DuplicateUserException {
		if(hasDuplicateSubjects(fanaticisms)) {
			throw new RepeatedFanaticismsException();
		}
		userCol.register(userKind, userId, fanaticisms);

		bySub.register(fanaticisms, userCol.getUser(userId));
	}

	public void addFriend (String user1Id, String user2Id) throws DuplicateIdException, InvalidUserException, DuplicateFriendException{
		if (user1Id.equals(user2Id)) {
			throw new DuplicateIdException(user1Id, user2Id);
		}

		userCol.addFriend(user1Id, user2Id);
	}

	public void post (String userId, int numHash, List<String> hashtags, String truthfulness, String message) throws InvalidUserException, InvalidHashtagsException, InadequatePostStanceException{
		if(hasDuplicateHashtags(hashtags) || numHash < 0) {
			throw new InvalidHashtagsException();
		}
		userCol.post(userId, hashtags, truthfulness, message);
		bySub.post(hashtags, getPost(userId, getLatestPostId(userId)));
	}

	public void comment(String commenterId, String userId, int postId, String stance, String comment) throws InvalidUserException, InexistentPostException, PostNotReceivedException, SelfcenteredException, InadequateCommentStanceException {
		userCol.comment(commenterId, userId, postId, stance, comment);
	}

	//================================================================================

	public int getFriendsAmount(String userId) {
		return userCol.getFriendsAmount(userId);
	}

	public int getLatestPostId(String userId) {
		return userCol.getLatestPostId(userId);
	}

	//================================================================================

	public User topPoster() throws NoElementsException {
		return userCol.topPoster();
	}

	public User responsive() throws NoElementsException {
		return userCol.responsive();
	}

	public User shameless() throws NoElementsException {
		return userCol.shameless();
	}
	
	public Post popularPost() throws NoElementsException {
		return bySub.popularPost();
	}

	public Post getPost(String userId, int postId) throws InvalidUserException, InexistentPostException {
		return userCol.getPost(userId, postId);
	}

	//================================================================================

	public Iterator<User> userIterator() throws NoUsersException {
		return userCol.userIterator();
	}

	public Iterator<User> friendIterator(String userId) throws InvalidUserException, NoFriendsException {
		return userCol.friendIterator(userId);
	}

	public Iterator<User> topicFanatics (String subject) throws InvalidFanaticismException {
		return bySub.topicFanatics(subject);
	}
	
	public Iterator<Post> topicPosts(String subject, int elems) throws InvalidElementNumberException, InvalidSubjectException {
		if (elems <= 0) {
			throw new InvalidElementNumberException();
		}
		return bySub.topicPosts(subject, elems);
	}

	public Iterator<Post> postIterator(String userId) throws InvalidUserException, NoPostsException {
		return userCol.postIterator(userId);
	}

	public Iterator<Comment> readpost (String userId, int postId) throws NoCommentsException {
		return userCol.readpost(userId, postId);
	}

	public Iterator<Comment> userComments(String userId, String subject) throws InvalidUserException, NoCommentsException {
		return userCol.userComments(userId, subject);
	}

	//================================================================================

	/**
	 * Checks if the given <code>Fanaticism List</code> contains any duplicates.
	 * 
	 * @param fanaticisms - <code>List</code> of <code>Fanaticism</code>.
	 * @return - true if the <code>List</code> contains any duplicates.
	 */
	private boolean hasDuplicateSubjects(List<Fanaticism> fanaticisms) {
		Set<String> set = new HashSet<String>(fanaticisms.size());
		boolean hasDuplicates = false;

		for (Fanaticism fan : fanaticisms) {
			set.add(fan.getSubject());
		}

		if (set.size() != fanaticisms.size()) {
			hasDuplicates = true;
		}

		return hasDuplicates;
	}

	/**
	 * Checks if the given <code>String List</code> contains any duplicates.
	 * 
	 * @param hashtags - <code>List</code> of <code>Strings</code>, representing the topics of a <code>Post</code>.
	 * @return - true if the <code>List</code> contains any duplicates.
	 */
	private boolean hasDuplicateHashtags(List<String> hashtags) {
		Set<String> set = new HashSet<String>(hashtags.size());
		boolean hasDuplicates = false;

		for (String hashtag: hashtags) {
			set.add(hashtag);
		}

		if (set.size() != hashtags.size()) {
			hasDuplicates = true;
		}

		return hasDuplicates;
	}
}

package network.topLevel.bySubject;

import java.util.Iterator;
import java.util.List;

import network.interactions.posts.Post;
import network.user.User;
import network.user.userKinds.fanatic.Fanaticism;
import utilities.exceptions.InvalidFanaticismException;
import utilities.exceptions.InvalidSubjectException;
import utilities.exceptions.NoElementsException;

/**
 * Responsible for storing all the posts of the social network. This makes it easier to access all posts if we dont know who posted them or just want to 
 * take information of how many posts there are and their popularity by comments.
 * 
 * @author Manuel Pereira
 * @author Mauricio Polvora
 */
public interface BySubject {

	/**
	 * Registers a fanatic <code>User</code> in the database. This makes it easier to then develop the command <code>subjectFanatics</code>
	 * 
	 * @param fanaticisms - Its list of fanaticisms.
	 * @param user - The fanatic
	 */
	public void register (List<Fanaticism> fanaticisms, User user);

	/**
	 * Registers a <code>post</code> by its <code>subjects</code>
	 * 
	 * @param subjects - The <code>post</code> subjects
	 * @param post - The <code>post</code> itself
	 */
	public void post (List<String> subjects, Post post);

	//================================================================================

	/**
	 * Returns the most popular <code>post</code>. The post with the highest number of comments. 
	 * 
	 * @return - The most popular <code>post</code>
	 * @throws NoElementsException- thrown if there are no <code>posts</code>
	 */
	public Post popularPost() throws NoElementsException;

	//================================================================================

	/**
	 * Returns the Iterator of the fanatics of a given <code>subject</code>
	 * 
	 * @param subject - The subject we want to get the fanatics from
	 * @return - The iterator of the fanatics of a given <code>subject</code>
	 * @throws InvalidFanaticismException - thrown if there are no fanatics from the given subject.
	 */
	public Iterator<User> topicFanatics (String subject) throws InvalidFanaticismException;

	/**
	 * Returns the iterator of the <code>Posts</code> that have the given <code>subject</code>. It sends a maximum of <code>elems</code> <code>Posts</code>
	 * 
	 * @param subject
	 * @param elems
	 * @return
	 * @throws InvalidSubjectException
	 */
	public Iterator<Post> topicPosts(String subject, int elems) throws InvalidSubjectException;

}

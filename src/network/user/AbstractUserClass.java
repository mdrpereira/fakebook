package network.user;
import java.util.List;

public abstract class AbstractUserClass implements User {
	private String kind, id;
	private int friendsAmt, postsAmt, commentsAmt, availablePosts, commentedPosts, totalLies;
		
	//================================================================================

	public AbstractUserClass (String userKind, String userId) {
		this.kind = userKind;
		this.id = userId;
	}
	
	//================================================================================

	public void addFriend() {
		friendsAmt++;
	}
	
	public void post(List<String> hashtags, String truthfulness) {
		postsAmt++;
	}
	
	public void comment() {
		commentsAmt++;
	}
	
	public void setAvailablePosts (int amount) {
		availablePosts = amount;
	}
	
	public void setCommentedPosts (int amount) {
		commentedPosts = amount;
	}
	
	public void setTotalLies (int amount) {
		totalLies = amount;
	}
	
	//================================================================================
	
	public String getUserId() {
		return id;
	}
	
	public String getUserKind() {
		return kind;
	}
	
	//================================================================================
	
	public int getFriendsAmount() {
		return friendsAmt;
	}
	
	public int getPostsAmount() {
		return postsAmt;
	}
	
	public int getCommentsAmount() {
		return commentsAmt;
	}
	
	public int getAvailablePosts() {
		return availablePosts;
	}
	
	public int getCommentedPosts() {
		return commentedPosts;
	}
	
	public int getTotalLies() {
		return totalLies;
	}
}

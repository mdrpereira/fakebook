package network.user;

import java.util.List;
import utilities.exceptions.InadequatePostStanceException;

/**
 * Class that stores <code>User</code> variables and counters.
 * 
 * @author Manuel Pereira
 * @author Mauricio Polvora
 */

public interface User {
	
	/**
	 * Adds a Friend to the <code>User</code> friends counter
	 * 
	 */
	public void addFriend();
	
	/**
	 * Adds a post to the <code>User</code> post counter. (we send hashtags and truthfulness but its only used in fanatic)
	 * 
	 * @param hashtags
	 * @param truthfulness
	 * @throws InadequatePostStanceException
	 */
	public void post(List<String> hashtags, String truthfulness) throws InadequatePostStanceException;
	
	/**
	 * Adds a comment to the <code>User</code> comment counter
	 * 
	 */
	public void comment();
	
	/**
	 * Sets the number of available posts of the <code>User</code>
	 * 
	 * @param amount - Number of posts.
	 */
	public void setAvailablePosts(int amount);
	
	/**
	 * Sets the number of commented posts of the <code>User</code> 
	 * 
	 * @param amount - Number of comments
	 */
	public void setCommentedPosts(int amount);
	
	/**
	 * Sets the number of lies of the <code>User</code>
	 * 
	 * @param amount
	 */
	public void setTotalLies (int amount);
	
	//================================================================================

	/**
	 * @return-The <code>User</code> id
	 */
	public String getUserId();
	
	/**
	 * @return - The <code>User</code> kind
	 */
	public String getUserKind();
	
	//================================================================================
	
	/**
	 * @return - The <code>User</code> friend number
	 */
	public int getFriendsAmount();
	
	/**
	 * @return - The <code>User</code> post amount
	 */
	public int getPostsAmount();
	
	/**
	 * @return - The <code>User</code> comments amount
	 */
	public int getCommentsAmount();
	
	/**
	 * @return - The <code>User</code> available posts
	 */
	public int getAvailablePosts();
	
	/**
	 * @return - The <code>User</code> comented posts amount
	 */
	public int getCommentedPosts();
	
	/**
	 * @return - the <code>User</code> total lies
	 */
	public int getTotalLies();
}

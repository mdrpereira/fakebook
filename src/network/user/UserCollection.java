package network.user;

import java.util.Iterator;
import java.util.List;

import network.interactions.comments.Comment;
import network.interactions.posts.Post;
import network.user.userKinds.fanatic.Fanaticism;
import utilities.exceptions.DuplicateFriendException;
import utilities.exceptions.DuplicateUserException;
import utilities.exceptions.InadequateCommentStanceException;
import utilities.exceptions.InadequatePostStanceException;
import utilities.exceptions.InexistentPostException;
import utilities.exceptions.InvalidHashtagsException;
import utilities.exceptions.InvalidUserException;
import utilities.exceptions.NoCommentsException;
import utilities.exceptions.NoElementsException;
import utilities.exceptions.NoFriendsException;
import utilities.exceptions.NoPostsException;
import utilities.exceptions.NoUsersException;
import utilities.exceptions.PostNotReceivedException;
import utilities.exceptions.SelfcenteredException;

public interface UserCollection {

	/**
	 * Registers a new user in the network. 
	 * 
	 * @param userKind - The type of user that will be registered (except fanatic which we deal with it differently)
	 * @param userId - The unique user id
	 * @throws DuplicateUserException - thrown if <code>userId</code> to be added is repeated.
	 */
	public void register (String userKind, String userId) throws DuplicateUserException;

	/**
	 * Registers a new fanatic user into the newtwork.
	 * 
	 * @param userKind - The type will always be fanatic
	 * @param userId - The unique user id
	 * @param fanaticisms -  List of the fanaticisms of the user. This consists of a sequence of pairs made of their loved or hated things.
	 * @throws DuplicateUserException thrown if <code>userId</code> to be added is repeated.
	 */
	public void register (String userKind, String userId, List<Fanaticism> fanaticisms) throws DuplicateUserException;

	/**
	 * Adds a new friend to a user. Created bidirectional relationship between the two given users.
	 * 
	 * @param user1Id - First user id.
	 * @param user2Id - Second user id.
	 * @throws InvalidUserException - thrown if <code>user1Id</code> or <code>user2Id</code> to be added are invalid.
	 * @throws DuplicateFriendException - thrown if friendship between <code>user1Id</code> and <code>user2Id</code> already exists.
	 */
	public void addFriend (String user1Id, String user2Id) throws InvalidUserException, DuplicateFriendException;

	/**
	 * User posts a new message to the network.
	 * 
	 * @param userId - The user Id that posted 
	 * @param numHash - The number of hashtags added to the post.
	 * @param hashtags - The hashtags added.
	 * @param truthfulness - Truthfullness of the post. 
	 * @param message - The message of the post itself.
	 * 
	 * @throws InvalidHashtagsException - thrown if <code>hashtags</code> to be added is invalid.
	 * @throws InadequatePostStanceException - thrown if the post stance contradicts the user stance.
	 */
	public void post (String userId, List<String> hashtags, String truthfulness, String message) throws InvalidUserException, InadequatePostStanceException ;

	/**
	 * User comments on a post in the network.
	 * 
	 * @param commenterId - The user id that will comment
	 * @param userId - The user id of the author of the post that will be commented.
	 * @param postId - The post id that will be commented
	 * @param stance - Whether the comment is positive or negative
	 * @param comment - The comment itself 
	 * 
	 * @throws InvalidUserException - thrown if <code>commenterId</code> or <code>userId</code> are invalid.
	 * @throws InexistentPostException - thrown if <code>postId</code> to be commented is invalid.
	 * @throws PostNotReceivedException -thrown if <code>commenterId</code> didnt receive the post. (<code>commenterId</code> and <code>userId</code> were not friends
	 * before the post was made.)
	 * @throws SelfcenteredException - thrown if <code>commenterId</code>
	 * @throws InadequateCommentStanceException - thrown if the post stance contradicts the user stance.
	 */
	public void comment(String commenterId, String userId, int postId, String stance, String comment) throws InvalidUserException, InexistentPostException, PostNotReceivedException, SelfcenteredException, InadequateCommentStanceException;

	//================================================================================

	/**
	 * Returns the amount of friends a given <code>User</code> has.
	 * 
	 * @param userId - The user id that we want the number of friends from
	 * @return - The number of friends (int)
	 */
	public int getFriendsAmount(String userId);

	/**
	 * Returns a given <code>User</code> last post Id
	 * 
	 * @param userId - The user id that we want the last post id from
	 * @return - The post id (int)
	 */
	public int getLatestPostId(String userId);

	//================================================================================

	/**
	 * Returns the <code>User</code> with the biggest amount of posts
	 * 
	 * @return - <code>User</code> with the biggest amount of posts.
	 * @throws NoElementsException - thrown if there are no posts in the social network.
	 */
	public User topPoster() throws NoElementsException;

	/**
	 * Returns the <code>User</code> with the highest percentage of commented posts.
	 * 
	 * @return - <code>User</code> with the highest percentage.
	 * @throws NoElementsException - thrown if there are no posts in the social network.
	 */
	public User responsive() throws NoElementsException;

	/**
	 * Returns the <code>User</code> that is the top liar
	 * 
	 * @return - <code>User</code> with the highest amount of lies in the system.
	 * @throws NoElementsException - thrown if there are no posts in the social network.
	 */
	public User shameless() throws NoElementsException;

	/**
	 * Returns a specific <code>User</code>
	 * 
	 * @param userId - The <code>User</code> id we want to return
	 * @return - The wanted <code>User</code>
	 */
	public User getUser(String userId);

	/**
	 * Returns a specific post.
	 * 
	 * @param userId - The <code>User</code> id that posted the post.
	 * @param postId - The <code>Post</code> of the post we want to return
	 * @return - <code>Post</code> with the given details
	 * @throws InvalidUserException - thrown if <code>commenterId</code> or <code>userId</code> are invalid.
	 * @throws InexistentPostException - thrown if <code>postId</code> to be commented is invalid.
	 */
	public Post getPost(String userId, int postId) throws InvalidUserException, InexistentPostException ;

	//================================================================================

	/**
	 * Returns a <code>Iterator</code> containing all users in the social network.
	 * 
	 * @return <code>Iterator</code> of all users in the social network
	 * @throws NoUsersException - thrown if there are no users registered in the system
	 */
	public Iterator<User> userIterator() throws NoUsersException;

	/**
	 * Returns an <code>Iterator</code> containing all friends of a given user
	 * 
	 * @param userId - The <code>User</code> id that we want to get the friends from
	 * @return - <code>Iterator</code> of all friends of given user
	 * @throws InvalidUserException - thrown if <code>commenterId</code> or <code>userId</code> are invalid.
	 * @throws NoFriendsException - thrown if the user has no friends
	 */
	public Iterator<User> friendIterator(String userId) throws InvalidUserException, NoFriendsException;

	/**
	 * Returns an <code>Iterator</code> of the posts a given user has.
	 * 
	 * @param userId - The user we want to get all the posts from
	 * @return - <code>Iterator</code> of all <code>userId</code> posts.
	 * @throws InvalidUserException - thrown if <code>userId</code> is invalid.
	 * @throws NoPostsException - thrown if there are no posts with the given subject.
	 */
	public Iterator<Post> postIterator(String userId) throws InvalidUserException, NoPostsException;

	/**
	 * Returns an <code>Iterator</code> of the comments of a post.
	 * 
	 * @param userId - The user that posted the post
	 * @param postId - The post id we want to access
	 * @return -<code>Iterator</code> of all comments of the post.
	 * @throws NoCommentsException - thrown if the post has no comments.
	 */
	public Iterator<Comment> readpost (String userId, int postId) throws NoCommentsException;

	/**
//	 * Returns an <code>Iterator</code> containing all specific user comments in all network <code>Posts</code>
	 * 
	 * @param userId - The user that posted the post
	 * @param subject - The <code>Post</code> subject we want to get the comments from
	 * @return - The comment iterator
	 * @throws InvalidUserException - thrown if <code>userId</code> is invalid.
	 * @throws NoCommentsException - thrown if the <code>User</code> has no comments.
	 */
	public Iterator<Comment> userComments(String userId, String subject) throws InvalidUserException, NoCommentsException;

}

package network.user;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import network.interactions.UserInteractions;
import network.interactions.UserInteractionsClass;
import network.interactions.comments.Comment;
import network.interactions.posts.Post;
import network.user.userKinds.Fanatic;
import network.user.userKinds.Liar;
import network.user.userKinds.Naive;
import network.user.userKinds.Selfcentered;
import network.user.userKinds.fanatic.Fanaticism;
import utilities.comparators.ResponsiveComparator;
import utilities.comparators.ShamelessComparator;
import utilities.comparators.TopPosterComparator;
import utilities.comparators.UserIdComparator;
import utilities.enumerates.OutputEnum;
import utilities.enumerates.UserKindEnum;
import utilities.enumerates.stances.CommentEnum;
import utilities.exceptions.DuplicateFriendException;
import utilities.exceptions.DuplicateUserException;
import utilities.exceptions.InadequateCommentStanceException;
import utilities.exceptions.InadequatePostStanceException;
import utilities.exceptions.InexistentPostException;
import utilities.exceptions.InvalidUserException;
import utilities.exceptions.NoCommentsException;
import utilities.exceptions.NoElementsException;
import utilities.exceptions.NoFriendsException;
import utilities.exceptions.NoPostsException;
import utilities.exceptions.NoUsersException;
import utilities.exceptions.PostNotReceivedException;
import utilities.exceptions.SelfcenteredException;

public class UserCollectionClass implements UserCollection {
	private Map<User, UserInteractions> users;

	//================================================================================

	public UserCollectionClass() {
		users = new TreeMap<User, UserInteractions>(new UserIdComparator());
	}

	//================================================================================

	@SuppressWarnings("incomplete-switch")
	public void register (String userKind, String userId) throws DuplicateUserException {
		UserKindEnum type = UserKindEnum.valueOf(userKind);

		if (userExists(userId)) {
			throw new DuplicateUserException(userId);
		}

		switch (type) {
		case liar:
			users.put(new Liar(userKind, userId) , new UserInteractionsClass());
			break;

		case naive:
			users.put(new Naive(userKind, userId) , new UserInteractionsClass());
			break;

		case selfcentered:
			users.put(new Selfcentered(userKind, userId) , new UserInteractionsClass());
			break;
		}
	}


	public void register (String userKind, String userId, List<Fanaticism> fanaticisms) throws DuplicateUserException {
		if (userExists(userId)) {
			throw new DuplicateUserException(userId);
		}
		users.put(new Fanatic(userKind, userId, fanaticisms) , new UserInteractionsClass());
	}

	public void addFriend (String user1Id, String user2Id) throws InvalidUserException, DuplicateFriendException{
		for (String id : List.of(user1Id, user2Id)) {
			if (!userExists(id)) {
				throw new InvalidUserException(id);
			}
		}
		try {
			User us1 = getUser(user1Id);
			User us2 = getUser(user2Id);

			users.get(us1).addFriend(us2);
			users.get(us2).addFriend(us1);

			us1.addFriend();
			us2.addFriend();
		} 
		catch (DuplicateFriendException dupFriendsEx) {
			throw new DuplicateFriendException(user1Id, user2Id);
		}
	}

	public void post (String userId, List<String> hashtags, String truthfulness, String message) throws InvalidUserException, InadequatePostStanceException {
		if (!userExists(userId)) {
			throw new InvalidUserException(userId);
		}

		User user = getUser(userId);
		UserInteractions ui = users.get(user);

		user.post(hashtags, truthfulness);
		ui.post(userId, hashtags, truthfulness, message);

		share(user, ui);
	}

	public void comment(String commenterId, String userId, int postId, String stance, String comment) throws InvalidUserException, InexistentPostException, PostNotReceivedException, SelfcenteredException, InadequateCommentStanceException {
		for (String id : List.of(commenterId, userId)) {
			if (!userExists(id)) {
				throw new InvalidUserException(id);
			}
		}

		User commenter = getUser(commenterId);
		User user = getUser(userId);

		UserInteractions userUi = users.get(user);
		UserInteractions commenterUi = users.get(commenter);


		if( postId < 1 || user.getPostsAmount() == 0 || userUi.getLastId() < postId) {
			throw new InexistentPostException(userId, postId);
		}

		Post post = userUi.getPost(postId);

		if( !commenter.equals(user) && (!commenterUi.hasSharedPost(post))) {
			throw new PostNotReceivedException(commenterId, postId, userId);
		}

		if((!commenterId.equals(userId)) && (commenter instanceof Selfcentered)) {
			throw new SelfcenteredException(commenterId);
		}
		if(!canComment(commenter, post, stance)) {
			throw new InadequateCommentStanceException();
		}

		commenter.comment();
		post.comment(commenterId, stance, comment);

		List<String> subjects = post.getHashtags();
		Comment commentObj = post.getLastComment();
		commenterUi.comment(subjects, commentObj);
	}


	//================================================================================

	public int getFriendsAmount(String userId) {
		User user = getUser(userId);
		return (user.getFriendsAmount());
	}

	public int getLatestPostId(String userId) {
		User user = getUser(userId);
		return (user.getPostsAmount());
	}

	//================================================================================

	//@Pre: Has user 
	public User getUser(String userId) {
		User found = null;

		for (User u : users.keySet()) {
			if (u.getUserId().equals(userId)) {
				return (found = u);
			}
		}
		return (found);
	}

	public User topPoster() throws NoElementsException {
		try {
			Set<User> topUsers = new TreeSet<User>(new TopPosterComparator());
			Iterator<User> it = userIterator();

			int totalPosts = 0;

			while (it.hasNext()) {
				User user = it.next();

				topUsers.add(user);
				totalPosts += user.getPostsAmount();
			}

			if (totalPosts == 0) {
				throw new NoElementsException(OutputEnum.NO_ELEM_TOPPOSTER.getMessage());
			}

			return ((TreeSet<User>) topUsers).first();
		}

		catch (NoUsersException ex) {
			throw new NoElementsException(OutputEnum.NO_ELEM_TOPPOSTER.getMessage());
		}
	}

	public User responsive() throws NoElementsException {
		try {
			Set<User> responsive = new TreeSet<User>(new ResponsiveComparator());


			for (User user: users.keySet()) {
				UserInteractions ui = users.get(user);

				Iterator<Post> it = ui.availablePosts();

				int availablePosts = 0, commentedPosts = 0;

				while (it.hasNext()) {
					Post post = it.next();
					availablePosts++;

					if (post.userCommented(user.getUserId())) {
						commentedPosts++;
					}
				}

				user.setAvailablePosts(availablePosts);
				user.setCommentedPosts(commentedPosts);

				if (availablePosts != 0 && commentedPosts != 0) {
					responsive.add(user);
				}
			}
			return ((TreeSet<User>) responsive).first();
		}
		catch (NoSuchElementException ex) {
			throw new NoElementsException(OutputEnum.NO_ELEM_RESPONSIVE.getMessage());
		}
	}


	public User shameless() throws NoElementsException {
		try {
			Set<User> shameless = new TreeSet<User>(new ShamelessComparator());

			for (User user: users.keySet()) {
				UserInteractions ui = users.get(user);

				Iterator<Post> postIt = ui.allPosts();
				Iterator<Comment> commentIt = ui.allComments();

				int totalLies = totalLies(postIt, commentIt);

				user.setTotalLies(totalLies);

				if (totalLies != 0) {
					shameless.add(user);
				}
			}
			return ((TreeSet<User>) shameless).first();
		}
		catch (NoSuchElementException ex) {
			throw new NoElementsException(OutputEnum.NO_ELEM_SHAMELESS.getMessage());
		}
	}

	public Post getPost(String userId, int postId) throws InvalidUserException, InexistentPostException {
		if (!userExists(userId)) {
			throw new InvalidUserException(userId);
		}

		User user = getUser(userId);
		UserInteractions userUi = users.get(user);

		if( postId < 1 || user.getPostsAmount() == 0|| userUi.getLastId() < postId) {
			throw new InexistentPostException(userId, postId);
		}

		return(userUi.getPost(postId));
	}

	//================================================================================

	public Iterator<User> userIterator() throws NoUsersException {
		Set<User> keys = users.keySet();

		if (keys.isEmpty()) {
			throw new NoUsersException();
		}
		return keys.iterator();
	}

	public Iterator<User> friendIterator(String userId) throws InvalidUserException, NoFriendsException {
		if (!userExists(userId)) {
			throw new InvalidUserException(userId);
		}
		UserInteractions ui = users.get(getUser(userId));

		try {
			return ui.friendIterator();
		}
		catch (NoFriendsException noFriendsEx) {
			throw new NoFriendsException(userId);
		}
	}

	public Iterator<Post> postIterator(String userId) throws InvalidUserException, NoPostsException {
		if (!userExists(userId)) {
			throw new InvalidUserException(userId);
		}
		UserInteractions ui = users.get(getUser(userId));

		try {
			return ui.postIterator();
		}
		catch (NoPostsException noPostsEx){
			throw new NoPostsException(userId);
		}
	}

	//@Pre: has checked for InvalidUserException and InexistentPostException
	public Iterator<Comment> readpost (String userId, int postId) throws NoCommentsException {
		User user = getUser(userId);
		UserInteractions userUi = users.get(user);
		Post post = userUi.getPost(postId);
		List<Comment> comments = post.getComments();

		if (comments.isEmpty()) {
			throw new NoCommentsException();
		}

		return comments.listIterator();
	}

	public Iterator<Comment> userComments(String userId, String subject) throws InvalidUserException, NoCommentsException {
		if (!userExists(userId)) {
			throw new InvalidUserException(userId);
		}
		UserInteractions ui = users.get(getUser(userId));

		return (ui.userComments(subject));
	}

	//================================================================================

	/**
	 * Shares a new post to all friends of a <code>User</code> 
	 * 
	 * @param user - Poster of the shared post
	 * @param ui - UserInteractions of the given poster
	 */
	private void share(User user, UserInteractions ui) {
		int postId = user.getPostsAmount();
		Post shared = ui.getPost(postId);

		Iterator<User> it = friendIterator(user.getUserId());

		while (it.hasNext()) {
			UserInteractions uiShare = users.get(it.next());

			uiShare.receive(shared);
		}
	}


	/**
	 * Calculates the total amount of lies a <code>User</code> has made. <br>
	 * For this, all posts and comments must be iterated through, so as to find for situations matching the requirements for a lie.
	 * 
	 * @param allPosts - <code>Iterator</code> of posts, that passes through all posts posted by the <code>User</code>.
	 * @param allComments - <code>Iterator</code> of comments, that passes though all comments made by the <code>User</code>.
	 * @return - Total lies made by the user as an <code>Integer</code>.
	 */
	private int totalLies (Iterator<Post> allPosts, Iterator<Comment> allComments) {
		int lies = 0;

		while (allPosts.hasNext()) {
			Post post = allPosts.next();

			if (!post.isTruthful()) {
				lies++;
			}
		}

		while (allComments.hasNext()) {
			Comment comment = allComments.next();

			if (comment.isPositive() != comment.postIsTruthful()) {
				lies++;
			}
		}
		return lies;
	}

	/**
	 * Uses the <code>Map</code> functionality <code>keySet()</code> to obtain an iterable object, as to check if a given <code>userId</code> has a corresponding user stored.
	 * 
	 * @param userId - Identifier of the user being checked for.
	 * @return - true if user already exists
	 */
	private boolean userExists(String userId) {
		boolean userExists = false;

		for (User key: users.keySet()) {
			
			if (key.getUserId().equals(userId)) {
				userExists = true;
			}
		}
		return userExists;
	}

	/**
	 * Uses the given preconditions to check if a given <code>User</code> can make a comment on the given <code>Post</code>. <br>
	 * 
	 * If the user is self-centered, or naive, this check is made by simply equaling the output of the method to the stance of the <code>Comment</code>. As naive users can only make positive comments, and similarly, self-centered users 
	 * can only comment positively on their own posts. <br>
	 * 
	 * Next, if the user is a liar, the <code>truthfulness</code> of the <code>Post</code> must be opposite to the stance of the <code>Comment</code>. <br>
	 * 
	 * Lastly, if the <code>User</code> is a <code>Fanatic</code>, the <code>Fanaticism<code> of the user must be iterated through, as to check if there are corresponding hashtags/fanaticisms. If so, then the following requirements 
	 * must be met :  If the <code>Post</code> is truthful, then the stance of the <code>Comment</code> must be the opposite of the fanaticism. If the <code>Post</code> is not truthful, then the stance of the <code>Comment</code>
	 * must match that of the <code>Fanaticism</code>.
	 * 
	 * @param user - Commenter of the message.
	 * @param post - <code>Post</code> to be commented on.
	 * @param commentStance - stance of the <code>Comment</code> being made.
	 * @return - true if the <code>User</code> can comment the given <code>Post</code>. 
	 */
	private boolean canComment(User user, Post post, String commentStance ) {
		boolean canComment = true, 
				commentIsPositive = CommentEnum.isPositive(commentStance);

		if (user instanceof Selfcentered || user instanceof Naive) {
			canComment = commentIsPositive;
		}
		else if (user instanceof Liar) {

			if (post.isTruthful() && commentIsPositive || !post.isTruthful() && !commentIsPositive) {
				canComment = false;
			}
		}
		else { //Represents an instance of Fanatic
			Fanaticism fan = ((Fanatic) user).matchingHashtags(post.getHashtags());

			if ((post.isTruthful() && (commentIsPositive != fan.isPositive())) || (!post.isTruthful() && (commentIsPositive == fan.isPositive()))) {
				canComment = false;
			}
		}
		return (canComment);
	}

}

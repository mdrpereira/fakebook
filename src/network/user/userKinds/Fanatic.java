package network.user.userKinds;

import java.util.ArrayList;
import java.util.List;

import network.user.AbstractUserClass;
import network.user.userKinds.fanatic.Fanaticism;
import utilities.enumerates.stances.ValidStancesEnum;
import utilities.exceptions.InadequatePostStanceException;

public class Fanatic extends AbstractUserClass {
	private List<Fanaticism> fanaticisms;

	public Fanatic(String userKind, String userId, List<Fanaticism> fanaticisms) {
		super(userKind, userId);
		this.fanaticisms = new ArrayList<Fanaticism>(fanaticisms);
	}

	@Override
	public void post(List<String> hashtags, String truthfulness) throws InadequatePostStanceException {
		if (!hasHashtag(hashtags) || !adequateHashtags(hashtags, truthfulness)) {
			throw new InadequatePostStanceException();
		}
		super.post(hashtags, truthfulness);
	}
	
	public Fanaticism matchingHashtags(List<String> hashtags) {
		Fanaticism fanaticism = null;
		
		loop:
		for (String hashtag: hashtags) {
			for (Fanaticism fan: fanaticisms) {
				if (fan.getSubject().equalsIgnoreCase(hashtag)) {
					fanaticism = fan;
					break loop;
				}
			}
		}
		return(fanaticism);
	}
	

	private boolean hasHashtag(List<String> hashtags) {
		boolean hasHashtag = false;

		loop:
			for (String hashtag: hashtags) {
				for (Fanaticism fan: fanaticisms) {
					if (fan.getSubject().equalsIgnoreCase(hashtag)) {
						hasHashtag = true;
						break loop;
					}
				}
			}
		return (hasHashtag);
	}

	private boolean adequateHashtags(List<String> hashtags, String truthfulness) {
		boolean areAdequate = true;

		loop:
			for(String hashtag: hashtags) {
				for (Fanaticism fan: fanaticisms) {
					if (fan.getSubject().equalsIgnoreCase(hashtag)) {
						if(!ValidStancesEnum.isStanceValid(fan.getStance(), truthfulness)) {
							areAdequate = false;
							break loop;
						}
					}
				}
			}
		return areAdequate;
	}
}

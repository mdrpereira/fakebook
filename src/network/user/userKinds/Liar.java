package network.user.userKinds;
import java.util.List;

import network.user.AbstractUserClass;
import utilities.enumerates.stances.PostEnum;
import utilities.exceptions.InadequatePostStanceException;

public class Liar extends AbstractUserClass{

	public Liar(String userKind, String userId) {
		super(userKind, userId);
	}
	
	//================================================================================
	
	@Override
	public void post(List<String> hashtags, String truthfulness) {
		
		if (PostEnum.isHonest(truthfulness)) {
			throw new InadequatePostStanceException();
		}
		super.post(hashtags, truthfulness);
	}
}

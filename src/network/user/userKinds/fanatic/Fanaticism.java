package network.user.userKinds.fanatic;

/**
 * Class that stores a <code>Fanatic</code> fanaticisms.
 * 
 * @author Manuel Pereira
 * @author Mauricio Polvora
 */
public interface Fanaticism {

	/**
	 * @return - The subjects the <code>Fanatic</code> is fanatic with
	 */
	public String getSubject();
	
	/**
	 * @return - The stance to the subject-
	 */
	public String getStance();
	
	//================================================================================
	
	/**
	 * @return - ??
	 */
	public boolean isPositive();
}

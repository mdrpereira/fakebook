package network.user.userKinds.fanatic;

import utilities.enumerates.stances.FanaticismEnum;

public class FanaticismClass implements Fanaticism{
	private String stance, subject;
	
	//================================================================================
	
	public FanaticismClass(String stance, String subject) {
		this.stance = stance;
		this.subject = subject;
	}
	
	//================================================================================
	
	public String getSubject() {
		return subject;
	}
	
	public String getStance() {
		return stance;
	}
	
	//================================================================================
	
	public boolean isPositive() {
		return (FanaticismEnum.loves(stance));
	}
	
}

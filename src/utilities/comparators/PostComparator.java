package utilities.comparators;

import java.util.Comparator;

import network.interactions.posts.Post;

/**
 * 
 * Compares two posts starting with comment amount follower with poster id and then post Id
 *
 */
public class PostComparator implements Comparator<Post> {

	public int compare (Post a, Post b) {
		int i = a.getCommentsAmount() - b.getCommentsAmount();
		if (i != 0) 
			return -i;

		i = a.getPosterId().compareTo(b.getPosterId());
		if (i != 0) 
			return i;

		i = a.getPostId() - b.getPostId();
		return -i;
	}
}

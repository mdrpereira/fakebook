package utilities.comparators;

import java.util.Comparator;

import network.user.User;

/**
 * 
 * Comparator that compares two users by their amount of commented posts and then their user Id
 *
 */
public class ResponsiveComparator implements Comparator<User>{

	public int compare (User a, User b) {
		int i = toPercentage(a) - toPercentage(b);
		if (i != 0)
			return -i;
		
		int j = a.getUserId().compareTo(b.getUserId());
		return j;
	}

	private int toPercentage(User user) {
		return (( user.getCommentedPosts() * 100) / ( user.getAvailablePosts()));
	}
}


package utilities.comparators;

import java.util.Comparator;

import network.user.User;

/**
 * Comparator that compares two users by their total lies followed by their post amount plus comments.
 *
 */
public class ShamelessComparator implements Comparator<User> {

	public int compare (User a, User b) {
		int i = a.getTotalLies() - b.getTotalLies();
		if (i != 0)
			return -i;

		i = ((a.getPostsAmount() + a.getCommentsAmount()) - (b.getPostsAmount() + b.getCommentsAmount()));
		if (i != 0)
			return i;

		i = a.getUserId().compareTo(b.getUserId());
		return i;
	}
}

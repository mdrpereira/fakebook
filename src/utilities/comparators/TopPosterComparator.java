package utilities.comparators;

import java.util.Comparator;

import network.user.User;

public class TopPosterComparator implements Comparator<User> {

	public int compare (User a, User b) {
		int i = a.getPostsAmount() - b.getPostsAmount();
		if (i != 0)
			return -i;
		
		i = a.getCommentsAmount() - b.getCommentsAmount();
		if (i != 0)
			return -i;
		
		i = a.getUserId().compareTo(b.getUserId());
		return i;
	}
}

package utilities.comparators;

import java.util.Comparator;

import network.user.User;

/**
 * 
 * Simply compares two <code>users</code> by their id
 *
 */
public class UserIdComparator implements Comparator<User> {

	public int compare(User a, User b) {
		return (a.getUserId().compareTo(b.getUserId()));
	}
}

package utilities.enumerates;


/**
 * Responsible for holding all system-level method related information, such as name and description of said command.
 * <p>
 * Command name and description in place to simplify <code>Main</code> class, both in initialization of <code>static final Strings</code>, purposed for command/method prompts, 
 * and the listing of all commands, such as with method <code>help</code>. <br>
 * This class requires no instantiation.
 * 
 * @author Manuel Pereira
 * @author Mauricio Polvora 
 *
 */
public enum CommandEnum {

	/** All program-level methods */
	REGISTER 		("registers a new user"),
	USERS 			("lists all users"),
	ADDFRIEND		("adds a new friend"),
	FRIENDS			("lists the user friends"),
	POST			("posts a new message"),
	USERPOSTS		("lists all posts by a user"),
	COMMENT			("user comments on a post"),
	READPOST  		("prints detailed info on a post"),
	COMMENTSBYUSER  ("shows all the comments by a user on a given post"),
	TOPICFANATICS	("shows a list of fanatic users on a topic"),
	TOPICPOSTS  	("shows a list of posts on a given topic"),
	POPULARPOST  	("shows the most commented post"),
	TOPPOSTER  		("shows the user with more posts"),
	RESPONSIVE  	("shows the user with a higher percentage of commented posts"),
	SHAMELESS 		("shows the top liars"),
	HELP 			("shows the available commands"),
	EXIT  			("terminates the execution of the program");

	//================================================================================
	
	/**
	 * Command description.
	 */
	private final String description;

	//================================================================================

	/**
	 * Single constructor for <code>Commands</code> class.
	 * @param description - the description of the command, used solely in <code>help</code> method.
	 */
	private CommandEnum(String description) {
		this.description = description;
	}

	//================================================================================
	
	/**
	 * Returns a <code>String</code> containing the description of the current Command, interpreted by the Enumerate name.
	 * @return the description of the current Command.
	 */
	public String getDescription() {
		return(description);
	}
	
	/**
	 * 
	 * @return
	 */
	public String getName() {
		return(name().toLowerCase());
	}

}

package utilities.enumerates;

/**
 * 
 * @author Manuel Pereira
 * @author Mauricio Polvora
 */
public enum OutputEnum {

	//Success out
	REGISTERED			(" registered."),
	FRIEND_ADDED		(" is friend of "),
	POSTED_1			(" sent a "),
	POSTED_2 			(" post to "),
	POSTED_3			(" friends. Post id = "),
	COMMENTED			("Comment added!"),
	
	//--------------------------------------------------------------------------------

	//Unsuccessful out
	INVALID_COMMAND 		("Unknown command. Type help to see available commands.\n"),
	INVALID_USER_KIND 		(" is an invalid user kind!"),
	INVALID_FANATICISMS 	("Invalid fanaticism list!"),
	DUPLICATE_USER			(" already exists!"),
	
	//---------------------------------------------

	NO_USERS				("There are no users!"),
	
	//---------------------------------------------

	INVALID_USER_ID			(" does not exist!"),
	DUPLICATE_ID			(" cannot be the same as "),
	DUPLICATE_FRIENDS		(" must really admire "),

	//---------------------------------------------
	
	NO_FRIENDS				(" has no friends!"),

	//---------------------------------------------
	
	INVALID_HASHTAGS		("Invalid hashtags list!"),
	INADEQUATE_STANCE		("Inadequate stance!"),

	//---------------------------------------------
	
	NO_POSTS				(" has no posts!"),

	//---------------------------------------------
	
	INEXISTENT_POST			(" has no post "),
	NO_ACCESS_1				(" has no access to post "),
	NO_ACCESS_2  			(" by "),
	CANNOT_COMMENT			(" cannot comment on this post!"),
	INVALID_COMMENT_STANCE	("Invalid comment stance!"),

	//---------------------------------------------
	
	NO_COMMENTS				("No comments!"),

	//---------------------------------------------
	
	INVALID_ELEMENTS_AMT	("Invalid number of posts to present!"),
	INVALID_SUBJECT			("Oh please, who would write about "),

	//---------------------------------------------
	
	INVALID_FANATICISM		("Oh please, who would be a fanatic of "),

	//---------------------------------------------
	
	NO_ELEM_BASE			("Social distancing has reached fakebook. "),
	NO_ELEM_POPULAR_POSTS	( NO_ELEM_BASE + "Please post something."),
	NO_ELEM_TOPPOSTER		( NO_ELEM_BASE + "Post something to become the king of posters."),
	NO_ELEM_RESPONSIVE		( NO_ELEM_BASE + "Post something and then comment your own post to become the king of responsiveness."),
	NO_ELEM_SHAMELESS		( NO_ELEM_BASE + "Post a lie and become the king of liars."),

	//---------------------------------------------
	
	EXIT 				("Bye!\n");

	//================================================================================
	
	private final String message;

	//================================================================================
	
	/**
	 * Single constructor for <code>Commands</code> class.
	 * @param message - the message to be printed.
	 */
	private OutputEnum(String message) {
		this.message = message;
	}
	
	//================================================================================

	/**
	 * 
	 * @return
	 */
	public String getMessage() {
		return (message);
	}
}

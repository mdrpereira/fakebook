package utilities.enumerates;

/**
 * Holds all possible user kinds. <br>
 * TO be expanded with future implementations of user kinds.
 * @author Manuel Pereira
 *
 */
public enum UserKindEnum {

	naive,
	liar,
	fanatic,
	selfcentered;
	
	//================================================================================
	
	private UserKindEnum() {
	}
	
	//================================================================================
	
	public static boolean isValid (String userKind) {
		boolean belongs = false;
		
		for (UserKindEnum uk : UserKindEnum.values()) {
			if (userKind.equals(uk.name())) {
				belongs = true;
			}
		}
		return belongs;
	}
}

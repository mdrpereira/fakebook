package utilities.enumerates.stances;

public enum CommentEnum {
	
	positive(true),
	negative(false);

	//================================================================================
	
	private final boolean isPositive;
	
	//================================================================================
	
	private CommentEnum(boolean isPositive) {
		this.isPositive = isPositive;
	}
	
	//================================================================================
	
	public static boolean isPositive (String stance) {
		CommentEnum cs = valueOf(stance);
		
		return cs.isPositive;
	}
}

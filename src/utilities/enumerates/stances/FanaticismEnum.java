package utilities.enumerates.stances;

public enum FanaticismEnum {

	loves(true),
	hates(false);
	
	//================================================================================
	
	private final boolean likes;
	
	//================================================================================
	
	private FanaticismEnum(boolean likes) {
		this.likes = likes;
	}
	
	//================================================================================
	
	public static boolean loves(String stance) {
		FanaticismEnum fs = valueOf(stance);
		return fs.likes;
	}
}

package utilities.enumerates.stances;

public enum PostEnum {

	honest(true),
	fake(false);
	
	//================================================================================
	
	private final boolean isHonest;
	
	//================================================================================
	
	private PostEnum(boolean isHonest) {
		this.isHonest = isHonest;
	}
	
	//================================================================================
	
	public static boolean isHonest (String stance) {
		PostEnum ps = valueOf(stance);
		return (ps.isHonest);
	}
}

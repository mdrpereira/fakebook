package utilities.enumerates.stances;

public enum ValidStancesEnum {

	loves ("honest"),
	hates ("fake");
	
	//================================================================================
	
	private final String truthfulness;
	
	//================================================================================
	
	private ValidStancesEnum(String truthfulness) {
		this.truthfulness = truthfulness;
	}
	
	//================================================================================
	
	public String getTruthfulness() {
		return(truthfulness);
	}
	
	//================================================================================
	
	public static boolean isStanceValid(String stance, String truthfulness) {
		boolean isValid = false;
		
		switch(valueOf(stance)) {
		case loves:
			if(truthfulness.equalsIgnoreCase(loves.getTruthfulness())) {
				isValid = true;
			}
			break;
			
		case hates:
			if(truthfulness.equalsIgnoreCase(hates.getTruthfulness())) {
				isValid = true;
			}
			break;
		}
		return (isValid);
	}
}

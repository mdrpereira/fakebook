package utilities.exceptions;

import utilities.enumerates.OutputEnum;

public class DuplicateFriendException extends IllegalArgumentException {
	private static final String OUT = OutputEnum.DUPLICATE_FRIENDS.getMessage();
	
	public DuplicateFriendException() {
		super();
	}
	
	public DuplicateFriendException(String user1Id, String user2Id) {
		super(user1Id + OUT + user2Id + "!");
	}
}

package utilities.exceptions;

import utilities.enumerates.OutputEnum;

public class DuplicateIdException extends IllegalArgumentException {
	private static final String OUT = OutputEnum.DUPLICATE_ID.getMessage();
	
	public DuplicateIdException (String user1Id, String user2Id ) {
		super(user1Id + OUT + user2Id + "!");
	}
}

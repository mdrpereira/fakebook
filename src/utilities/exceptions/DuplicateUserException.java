package utilities.exceptions;

import utilities.enumerates.OutputEnum;

public class DuplicateUserException extends Exception {
	private static final String OUT = OutputEnum.DUPLICATE_USER.getMessage();
	
	public DuplicateUserException(String id) {
		super(id + OUT);
	}
}

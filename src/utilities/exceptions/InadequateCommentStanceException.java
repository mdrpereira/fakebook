package utilities.exceptions;

import utilities.enumerates.OutputEnum;

public class InadequateCommentStanceException extends IllegalArgumentException {
	private static final String OUT = OutputEnum.INVALID_COMMENT_STANCE.getMessage();
	
	public InadequateCommentStanceException() {
		super(OUT);
	}
}

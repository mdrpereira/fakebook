package utilities.exceptions;

import utilities.enumerates.OutputEnum;

public class InadequatePostStanceException extends IllegalArgumentException {
	private static final String OUT = OutputEnum.INADEQUATE_STANCE.getMessage();
	
	public InadequatePostStanceException() {
		super(OUT);
	}
}

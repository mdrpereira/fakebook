package utilities.exceptions;

import java.util.NoSuchElementException;

import utilities.enumerates.OutputEnum;

public class InexistentPostException extends NoSuchElementException {
	private static final String OUT = OutputEnum.INEXISTENT_POST.getMessage();
	
	public InexistentPostException(String userId, int postId) {
		super(userId + OUT + postId + "!");
	}
}

package utilities.exceptions;

import utilities.enumerates.OutputEnum;

public class InvalidElementNumberException extends IllegalArgumentException {
	private static final String OUT = OutputEnum.INVALID_ELEMENTS_AMT.getMessage();
			
	public InvalidElementNumberException () {
		super(OUT);
	}
}

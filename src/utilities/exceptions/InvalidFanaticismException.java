package utilities.exceptions;

import java.util.NoSuchElementException;

import utilities.enumerates.OutputEnum;

public class InvalidFanaticismException extends NoSuchElementException {
	private static final String OUT = OutputEnum.INVALID_FANATICISM.getMessage();
	
	public InvalidFanaticismException (String fanaticism) {
		super(OUT + fanaticism + "?");
	}
}

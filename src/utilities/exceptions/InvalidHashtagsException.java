package utilities.exceptions;

import utilities.enumerates.OutputEnum;

public class InvalidHashtagsException extends IllegalArgumentException {
	private static final String OUT = OutputEnum.INVALID_HASHTAGS.getMessage();
	
	public InvalidHashtagsException() {
		super(OUT);
	}
}

package utilities.exceptions;

import java.util.NoSuchElementException;

import utilities.enumerates.OutputEnum;

public class InvalidSubjectException extends NoSuchElementException {
	private static final String OUT = OutputEnum.INVALID_SUBJECT.getMessage();
			
	public InvalidSubjectException(String subject) {
		super(OUT + subject + "?");
	}
}

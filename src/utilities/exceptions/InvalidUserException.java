package utilities.exceptions;

import java.util.NoSuchElementException;

import utilities.enumerates.OutputEnum;

public class InvalidUserException extends NoSuchElementException {
	private static final String OUT = OutputEnum.INVALID_USER_ID.getMessage();
	
	public InvalidUserException(String id) {
		super(id + OUT);
	}
}

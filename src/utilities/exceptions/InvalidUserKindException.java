package utilities.exceptions;

import utilities.enumerates.OutputEnum;

public class InvalidUserKindException extends IllegalArgumentException{
	private static final String OUT = OutputEnum.INVALID_USER_KIND.getMessage();
	
	public InvalidUserKindException(String kind) {
		super(kind + OUT);
	}
}

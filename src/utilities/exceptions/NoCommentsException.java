package utilities.exceptions;

import java.util.NoSuchElementException;

import utilities.enumerates.OutputEnum;

public class NoCommentsException extends NoSuchElementException {
	private static final String OUT = OutputEnum.NO_COMMENTS.getMessage();
	
	public NoCommentsException() {
		super(OUT);
	}
}

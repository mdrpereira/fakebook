package utilities.exceptions;

import java.util.NoSuchElementException;

public class NoElementsException extends NoSuchElementException {
	
	public NoElementsException(String exception) {
		super(exception);
	}
}

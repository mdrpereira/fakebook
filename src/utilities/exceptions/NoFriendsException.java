package utilities.exceptions;

import java.util.NoSuchElementException;

import utilities.enumerates.OutputEnum;

public class NoFriendsException extends NoSuchElementException {
	private static final String OUT = OutputEnum.NO_FRIENDS.getMessage();
	
	public NoFriendsException() {
		super();
	}
	
	public NoFriendsException(String userId) {
		super(userId + OUT);
	}
	
}

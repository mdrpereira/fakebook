package utilities.exceptions;

import java.util.NoSuchElementException;

import utilities.enumerates.OutputEnum;

public class NoPostsException extends NoSuchElementException{
	private static final String OUT = OutputEnum.NO_POSTS.getMessage();
			
	public NoPostsException() {
		super();
	}
	
	public NoPostsException(String userId) {
		super(userId + OUT);
	}
}

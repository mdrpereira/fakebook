package utilities.exceptions;

import java.util.NoSuchElementException;

import utilities.enumerates.OutputEnum;

public class NoUsersException extends NoSuchElementException {
	private static final String OUT = OutputEnum.NO_USERS.getMessage();
	
	public NoUsersException() {
		super(OUT);
	}
}

package utilities.exceptions;

import java.util.NoSuchElementException;

import utilities.enumerates.OutputEnum;

public class PostNotReceivedException extends NoSuchElementException {
	private static final String OUT_1 = OutputEnum.NO_ACCESS_1.getMessage();
	private static final String OUT_2 = OutputEnum.NO_ACCESS_2.getMessage();
	
	public PostNotReceivedException(String commenterId, int postId, String userId) {
		super(commenterId + OUT_1 + postId + OUT_2 + userId + "!");
	}
	
}

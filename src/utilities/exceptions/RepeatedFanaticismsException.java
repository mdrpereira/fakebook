package utilities.exceptions;

import utilities.enumerates.OutputEnum;

public class RepeatedFanaticismsException extends IllegalArgumentException {
	private static final String OUT = OutputEnum.INVALID_FANATICISMS.getMessage();
	
	public RepeatedFanaticismsException() {
		super(OUT);
	}
}

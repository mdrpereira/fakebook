package utilities.exceptions;

import utilities.enumerates.OutputEnum;

public class SelfcenteredException extends IllegalArgumentException{
	private static final String OUT = OutputEnum.CANNOT_COMMENT.getMessage();
	
	public SelfcenteredException(String userId) {
		super(userId + OUT);
	}
}
